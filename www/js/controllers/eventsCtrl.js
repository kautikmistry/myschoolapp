angular.module('schoolApp.eventsCtrl', [])

.controller('eventsCtrl', ['$scope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicLoading', '$rootScope', '$cordovaToast', '$cordovaDevice', '$filter',
    function($scope, $stateParams, dataFactory, Config, $state, $localStorage, $ionicLoading, $rootScope, $cordovaToast, $cordovaDevice, $filter) {
        $ionicLoading.show({
            template: 'Loading Events...'
        });
        $scope.arr = $localStorage.StudentData;
        $scope.school_id = $stateParams.school_id
        $scope.student_id = $stateParams.student_id
        $rootScope.school_id = $stateParams.school_id
        //console.log("id root", $scope.school_id);
        
        $scope.currentPage = 0;
        $scope.pageSize = 10;
        $scope.q = '';

        dataFactory.getEvents($localStorage.contactNo, $scope.school_id, $scope.student_id)
        .success(function(response) {
            $ionicLoading.hide();
            $rootScope.events = response.data;
            //console.log($scope.events.length);
        })
        .error(function(error) {
             $cordovaToast
            .show('No events Found!', 'long', 'bottom')
                .then(function(success) {

                    console.log('Error');
                });
            $ionicLoading.hide();
        });

        // $scope.getData = function () {
        //     return $filter('filter')($rootScope.events, $scope.q)
        // }


        // $scope.numberOfPages=function(){
        //     return Math.ceil($scope.getData().length/$scope.pageSize);                
        // }   
    

        // dataFactory.saveInstallationId($localStorage.contactNo, $cordovaDevice.getUUID())
        // .success(function(response) {
        //     console.log("Installation Id successfully save..");
        // })
        // .error(function(error) {
        //     console.log(error);
        //     alert('error while save Installation Id. Please try again later.');
        //     $ionicLoading.hide();
        // });

        // $scope.moveToEvent = function(event) {
        //     $localStorage.defaultEvent = event;
        //     $state.go('/events/:schoolId/:eventId', {
        //         "schoolId": $scope.defaultSchool.school_id,
        //         "eventId": event.event_id
        //     });
        // }
    }
])