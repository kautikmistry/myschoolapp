angular.module('schoolApp.documentsCtrl', [])

.controller('documentsCtrl', ['$scope', '$rootScope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicLoading', '$ionicHistory', '$cordovaToast',
    function($scope, $rootScope, $stateParams, dataFactory, Config, $state, $localStorage, $ionicLoading, $ionicHistory, $cordovaToast) {
    	$ionicLoading.show({
            template: 'Loading Documents...'
        });
        $scope.student_id = $stateParams.student_id;
        //console.log($scope.student_id);
        dataFactory.getDocuments($scope.student_id)
        .success(function(response) {
        	$ionicLoading.hide();
        	console.log(response.data);
            $scope.documents = response.data;
        })
        .error(function(error) {
		 $cordovaToast
		.show('No Document Found!', 'long', 'bottom')
		    .then(function(success) {

		        console.log('Error');
		    });
		$ionicLoading.hide();
		});

        $scope.SaveFile = function(data){
            var file = new Blob([data], { type: 'application/pdf' });
            saveAs(file, 'filename.pdf');
        }
   	}
]);