angular.module('schoolApp.messagesDetailCtrl', [])

.controller('messagesDetailCtrl', ['$scope', '$rootScope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicLoading', '$ionicHistory', '$cordovaToast', '$cordovaInAppBrowser', '$sce', '$ionicPopup', '$filter', '$timeout', '$ionicScrollDelegate', '$window', '$anchorScroll', '$location',
function($scope, $rootScope, $stateParams, dataFactory, Config, $state, $localStorage, $ionicLoading, $ionicHistory, $cordovaToast, $cordovaInAppBrowser, $sce, $ionicPopup, $filter, $timeout, $ionicScrollDelegate, $window, $anchorScroll, $location) {

    $scope.senderid = $stateParams.senderid;
    $scope.receiverid = $stateParams.receiverid;
   //  $timeout(function() {
	  //   //$anchorScroll('myAnchor');
	  //   $location.hash('myAnchor');
	  //   //console.log($location.hash());
	  //   $anchorScroll();
	  // });
    dataFactory.receiveMsg($scope.senderid, $scope.receiverid)
        .success(function(response) {
            console.log(response);
            if (response.code == 401) {
                $scope.errmsg = "No Record Found!";
            } else {
                $scope.hideTime = true;
                console.log(response.data);
                 console.log($scope.senderid);
    			 console.log($scope.receiverid );
                $scope.msg = response.data;
                $scope.myId = $scope.receiverid;

            }
        })
        .error(function(error) {
            console.log(error);
            $scope.errmsg = "Chat history Not Found!";
        })

    //var isIOS = ionic.Platform.isWebView() && ionic.Platform.isIOS();

    $scope.sendMessage = function() {
    	console.log($scope.receiverid);
    	$scope.CurrentDate = $filter('date')(new Date(), 'dd-MM-yyyy HH:mm:ss');
    	

    	 dataFactory.SendMsg($scope.data.message, $scope.senderid, $scope.receiverid, $scope.CurrentDate)
	        .success(function(response) {
	        	console.log(response);
			     $cordovaToast
                .show('Message sent!', 'long', 'bottom')
                    .then(function(success) {
                    	$scope.msg.push({
					       user_id: $scope.receiverid,
					       msg: $scope.data.message,
					       time: $scope.CurrentDate
					     });
                    	console.log($scope.msg);
                    });
                return;

			    
	        })
	        .error(function(error) {
	        	console.log(error);
	        })
    }
    $scope.msg = [];

	$scope.inputUp = function() {
	    
	        $ionicScrollDelegate.scrollBottom(true);
	   

	};

	$scope.inputDown = function() {
	    //$scope.data.keyboardHeight = 0;
	    $ionicScrollDelegate.resize();
	};

	$scope.closeKeyboard = function() {
	    // cordova.plugins.Keyboard.close();
	};
}]);
