angular.module('schoolApp.studentsCtrl', [])


.controller('studentsCtrl', ['$scope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicLoading', '$rootScope', '$ionicPopup',
    function($scope, $stateParams, dataFactory, Config, $state, $localStorage, $ionicLoading, $rootScope, $ionicPopup) {
        dataFactory.saveInstallationId($localStorage.contactNo, $localStorage.installation_id)
            .success(function(response) {
                console.log(response);
            })
        .error(function(error) {
                console.log(error);
                $ionicLoading.hide();
            });
        $scope.data = $localStorage.StudentData;
        $scope.signOut = function() {
            $localStorage.$reset();
            $state.go("signin");
        }

        $scope.signOut = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'SignOut Alert!',
                template: 'Are you sure you want to Exit the App ?'
            });

            confirmPopup.then(function(res) {
                if (res) {
                     $localStorage.$reset();
                     $state.go("signin");
                } else {
                    console.log('You are not sure');
                }
            });
        };
        //console.log($location);
        $scope.doSomething = function(school_id, student_id) {
            $state.go('app.events', { 'school_id':school_id, 'student_id':student_id});
        }   
    }
])
