angular.module('schoolApp.pagesCtrl', [])

.controller('pagesCtrl', ['$scope', '$rootScope', '$sce', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicLoading', '$ionicHistory', '$cordovaToast', '$window',
    function($scope, $rootScope, $sce, $stateParams, dataFactory, Config, $state, $localStorage, $ionicLoading, $ionicHistory, $cordovaToast, $window) {
  		$ionicLoading.show({
            template: 'Loading Page...'
        });
        $scope.Data ={};
    	$scope.school_id = $stateParams.school_id;
    	$scope.page_id = $stateParams.id;
    	dataFactory.getStaticMenuContent($scope.school_id, $scope.page_id)
		.success(function(response) {
			$ionicLoading.hide();
			//console.log(response.data);
			$scope.Data.data = response.data;
			$scope.title = response.data.article_name;
			$scope.myHTML =$sce.trustAsHtml(response.data.article_description);
			$scope.$apply();
		})
		.error(function(error) {
		 $cordovaToast
		.show('No events Found!', 'long', 'bottom')
		    .then(function(success) {

		        console.log('Error');
		    });
		$ionicLoading.hide();
		});
    }
]);