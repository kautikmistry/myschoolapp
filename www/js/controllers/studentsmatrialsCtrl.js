angular.module('schoolApp.studentsmatrialsCtrl', [])

.controller('studentsmatrialsCtrl', ['$scope', '$rootScope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicLoading', '$ionicHistory', '$cordovaToast', '$cordovaInAppBrowser', '$sce', '$ionicPopup', '$filter',
    function($scope, $rootScope, $stateParams, dataFactory, Config, $state, $localStorage, $ionicLoading, $ionicHistory, $cordovaToast, $cordovaInAppBrowser, $sce, $ionicPopup, $filter) {
    	$ionicLoading.show({
            template: 'Loading Documents...'
        });
        $scope.class = $stateParams.class;
        console.log($scope.class);
        dataFactory.getStudentsMatrials($scope.class)
        .success(function(response) {
        	$ionicLoading.hide();
        	console.log(response.data);
            $scope.documents = response.data;
            $scope.trustAsHtml = $sce.trustAsHtml;
        })
        .error(function(error) {
		 $cordovaToast
		.show('No Document Found!', 'long', 'bottom')
		    .then(function(success) {
                $ionicLoading.hide();
		        console.log('Error');
		    });
		});

        $scope.SaveFile = function(data, type){
            //var file = new Blob([data], { type: 'application/pdf' });
            //saveAs(file, 'filename.pdf');
            var options = {
              location: 'yes',
              clearcache: 'yes',
              toolbar: 'no'
            };

            dataFactory.getObjectUrl(data)
               .success(function(response) {
                    console.log(response.data);
                    console.log(response.data.split('?AWSAccessKeyId')[0]);
                    var Image = ['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];
                    var Doc = ['pdf', 'doc', 'docx', 'xlsx', 'PDF', 'DOC', 'DOCX', 'XLSX', 'PPT','PPTX','ppt','pptx'];
                    var Audio = ['MP3', 'mp3'];
                    var Video = ['avi', 'AVI', 'MP4', 'mp4']; 
                    if(Image.indexOf(type) > -1 ){
                        $scope.aImages = response.data.split('?AWSAccessKeyId')[0];
                        var myPopup = $ionicPopup.show({
                            templateUrl: 'imagepopup-template.html',
                            title: 'Students Martial',
                            scope: $scope,

                            buttons: [{
                                text: 'Cancel'
                            }]
                        });
                        myPopup.then(function(res) {
                            
                        });
                    }else if(Doc.indexOf(type) > -1){
                        var fileUrl = "https://docs.google.com/gview?embedded=true&url=" + response.data.split('?AWSAccessKeyId')[0];
                         //var fileUrl = 'https://view.officeapps.live.com/op/embed.aspx?src='+ response.data.split('?AWSAccessKeyId')[0];
                         var script = "document.getElementsByClassName('ndfHFb-c4YZDc-Wrql6b-SmKAyb')[0].style.display = 'none'";
                         //$cordovaInAppBrowser.open(cordova.file.dataDirectory + "Documents/" + "sec.html", '_blank', options)
                         $cordovaInAppBrowser.open(fileUrl, '_blank', options)
                             .then(function(event) {
                                 // success
                                 $rootScope.$on('$cordovaInAppBrowser:loadstop', function(e, event) {
                                     $cordovaInAppBrowser.executeScript({
                                         code: script,
                                     });
                                 });
                             })
                             .catch (function(event) {
                                 // error
                             });
                    }else if(Audio.indexOf(type) > -1){
                        console.log(type);
                        alert("Audio");
                        $scope.aaudio = response.data.split('?AWSAccessKeyId')[0];
                        var myPopup = $ionicPopup.show({
                            templateUrl: 'audiopopup-template.html',
                            title: 'Students Martial',
                            scope: $scope,

                            buttons: [{
                                text: 'Cancel'
                            }]
                        });
                        myPopup.then(function(res) {
                            
                        });
                    }else if(Video.indexOf(type) > -1){
                        console.log(type);
                        alert("Not display video properly! Click on Full Screen Option.");
                        $scope.avideo = response.data.split('?AWSAccessKeyId')[0];
                        var myPopup = $ionicPopup.show({
                            templateUrl: 'videopopup-template.html',
                            title: 'Students Martial',
                            scope: $scope,

                            buttons: [{
                                text: 'Cancel'
                            }]
                        });
                        myPopup.then(function(res) {
                            
                        });
                    }
            })
            .error(function(error) {
             $cordovaToast
            .show('No Document Found!', 'long', 'bottom')
                .then(function(success) {
                    $ionicLoading.hide();
                    console.log('Error');
                });
            });
        }
   	}
])

.filter('groupBy', function() {
    return _.memoize(function(items, field) {
            return _.groupBy(items, field);
        }
    );
});