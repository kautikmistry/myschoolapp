angular.module('schoolApp.resultCtrl', [])

.controller('resultCtrl', ['$scope', '$rootScope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicLoading', '$ionicHistory', '$cordovaToast',
    function($scope, $rootScope, $stateParams, dataFactory, Config, $state, $localStorage, $ionicLoading, $ionicHistory, $cordovaToast) {
    	
        $scope.school_id = $stateParams.school_id;
        $scope.gr_number = $stateParams.gr_number;

        dataFactory.getExamsTakenBySchool($scope.school_id)
            .success(function(response){
                console.log(response);
                $scope.selectResult = response;
            })
            .error(function(error){

            });

        $scope.resultData = {};
        $scope.getExam = function(examForm) {
            if (!examForm.$valid) {
                // alert('Please enter the details.');
                $cordovaToast
                .show('Please enter the details!', 'long', 'bottom')
                    .then(function(success) {

                        console.log('Error');
                    });
                return;
            }
            
            console.log($scope.resultData.grno);
            console.log($scope.resultData.exam);

            dataFactory.getStudentResult($scope.school_id, $scope.resultData.grno, $scope.resultData.exam)
                .success(function(response) {
                    $ionicLoading.hide();
                    console.log(response.data);
                    $scope.results = response.data;
                })
                .error(function(error) {
                     $scope.results = "";
                     $cordovaToast
                    .show('No Results Found!', 'long', 'bottom')
                        .then(function(success) {

                            console.log('Error');
                        });
                    $ionicLoading.hide();
                });
         };
    }
]);
