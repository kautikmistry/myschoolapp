angular.module('schoolApp.messagesCtrl', [])

.controller('messagesCtrl', ['$scope', '$rootScope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicLoading', '$ionicHistory', '$cordovaToast', '$cordovaInAppBrowser', '$sce', '$ionicPopup', '$filter',
    function($scope, $rootScope, $stateParams, dataFactory, Config, $state, $localStorage, $ionicLoading, $ionicHistory, $cordovaToast, $cordovaInAppBrowser, $sce, $ionicPopup, $filter) {
      
    	$rootScope.Pid = $stateParams.parentid;
      	dataFactory.GetAllFaculty()
      		.success(function(response){
      			console.log(response);
      			$scope.faculty = response.data;
      		})
      		.error(function(error){
      			console.log(error);
      		})

		$scope.GetMessagesDetail = function(_id) {
			$state.go('app.messagesDetail', {'senderid':_id, 'receiverid':$rootScope.Pid});
		}
    }
]);