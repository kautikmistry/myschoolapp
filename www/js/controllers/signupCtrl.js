angular.module('schoolApp.signupCtrl', [])

.controller('signupCtrl', ['$scope', '$rootScope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicLoading', '$ionicHistory', '$cordovaToast',
    function($scope, $rootScope, $stateParams, dataFactory, Config, $state, $localStorage, $ionicLoading, $ionicHistory, $cordovaToast) {
        $scope.closeLogin = function() {
            $ionicHistory.goBack();
        };
        $scope.loginData = {};
        $scope.doLogin = function(userForm) {
            if (!userForm.$valid) {
                //alert('Please enter the details.');
                $cordovaToast
                .show('Please enter the details!', 'long', 'bottom')
                    .then(function(success) {

                        console.log('Error');
                    });
                return;
            }
            $ionicLoading.show({
                template: 'Verifying Details...'
            });

            dataFactory.isParentExists($scope.loginData.contactno)
                .success(function(response) {
                    $scope.acesskey = response.acesskey;
                    $scope.verification_code = response.verification_code;
                    dataFactory.verifyParent($scope.acesskey, $scope.verification_code)
                        .success(function(response) {
                            console.log("Parent successfully verified");
                            dataFactory.signUpParent($scope.acesskey, $scope.loginData.email, $scope.loginData.password)
                            if (response) {
                                console.log("Parent Successfully Registerd....");
                                console.log($scope.acesskey);
                                console.log($scope.loginData.email);
                                console.log($scope.loginData.password);
                                alert("Parent Successfully Registerd....");
                                $ionicLoading.hide();
                                $state.go('signin');
                            } else {
                                console.log("You are not a verified parent...");
                            }

                        })
                        .error(function(response) {
                            //console.log(error);
                            alert('Does not match verification code.');
                            $ionicLoading.hide();
                        })
                })
                .error(function(response) {
                    //console.log(error);
                    alert('parent Could not exists..');
                    $ionicLoading.hide();
                })

        }
    }
]);