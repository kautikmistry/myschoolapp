angular.module('schoolApp.signinCtrl', [])

.controller('signinCtrl', ['$scope', '$rootScope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicLoading', '$cordovaToast',
    function($scope, $rootScope, $stateParams, dataFactory, Config, $state, $localStorage, $ionicLoading, $cordovaToast) {
        $scope.singupData = {};
        if($localStorage.StudentData){
            $state.go('students', {}, {reload: true});
        }
        // if ($localStorage.schools) {
        //     dataFactory.getSchools($localStorage.contactno)
        //         .success(function(response) {
        //             $ionicLoading.hide();
        //             //console.log(response.data);
        //             if (response) {
        //                 $localStorage.schools = response.data;
        //                 $localStorage.defaultSchool = response.data[0];
        //                 params = {
        //                     'schoolId': $localStorage.defaultSchool.school_id
        //                 };
        //                 $state.go('app.events', params)
        //             } else {
        //                 alert('error while loading school details. Please try again later.');
        //                 if (callback) {
        //                     callback(false);
        //                 }
        //             }
        //         })
        //         .error(function(response) {
        //             //console.log(error);
        //             alert('error while loading school details. Please try again later.');
        //             $ionicLoading.hide();
        //         })
        //     params = {
        //         'schoolId': $localStorage.defaultSchool.school_id
        //     };
        //     $state.go('app.events', params)
        // }
        $scope.doSignUp = function(userForm) {
            if (!userForm.$valid) {
                // alert('Please enter the details.');
                $cordovaToast
                .show('Please enter the details!', 'long', 'bottom')
                    .then(function(success) {

                        console.log('Error');
                    });
                return;
            }
            $ionicLoading.show({
                template: 'Verifying Details...'
            });

            dataFactory.isParentExists($scope.singupData.contactno, $scope.singupData.password)
                .success(function(response) {
                    if (response) {
                        $localStorage.contactNo = response.acesskey;
                        dataFactory.parentLogin($localStorage.contactNo, $scope.singupData.password)
                            .success(function(response) {
                                console.log("Parent Successfully Loged in");
                                $ionicLoading.hide();
                                if (response) {
                                    dataFactory.getParentSchoolStudentData($localStorage.contactNo)
                                        .success(function(response) {
                                            if (response) {
                                                console.log("Details are found....");
                                                $localStorage.StudentData = response.data;
                                                console.log($localStorage.StudentData);
                                                $state.go('students');

                                            } else {
                                                alert("Sorry No Details Found Please Contact To Administrator..");
                                            }
                                        })
                                        .error(function(error) {
                                            alert('error while loading school details. Please try again later.');
                                            $ionicLoading.hide(); 
                                        });

                                    // $localStorage.schools = response.data;
                                    // $localStorage.defaultSchool = response.data[0];
                                    // params = { 'schoolId': $localStorage.defaultSchool.school_id };
                                    // $state.go('app.events', params)
                                } else {
                                    alert('error while loading school details. Please try again later.');
                                    if (callback) {
                                        callback(false);
                                    }
                                }
                            })
                            .error(function(error) {
                                //console.log(error);
                                alert('error while loading school details. Please try again later.');
                                $ionicLoading.hide();
                            });
                    } else {

                    }
                })
                .error(function(error) {
                    //console.log(error);
                    alert('Parent Could not exists.. Please try again later.');
                    $ionicLoading.hide();
                });
            //console.log(device.uuid);
            // dataFactory.parentExists($scope.singupData.contactno, $scope.singupData.name, 'Abcdeg169789842dfgjhkhioognbiynkloiyuhkldsfouoiuosdfhiu')
            //     .success(function(response) {
            //         $ionicLoading.hide();
            //         $ionicLoading.show({
            //           template: 'Loading...'
            //         });
            //         console.log('response.data.acesskey:' + response.acesskey);
            //         if (response) {
            //             $localStorage.contactno = response.acesskey;
            //             dataFactory.verifyMobile($localStorage.contactno, 'uhuneb')
            //                 .success(function(response) {
            //                     $ionicLoading.hide();
            //                     //console.log(response.data);
            //                     if (response) {
            //                         $localStorage.schools = response.data;
            //                         $localStorage.defaultSchool = response.data[0];
            //                         params = { 'schoolId': $localStorage.defaultSchool.school_id };
            //                         $state.go('app.events', params)
            //                     } else {
            //                         alert('error while loading school details. Please try again later.');
            //                         if (callback) {
            //                             callback(false);
            //                         }
            //                     }
            //                 })
            //                 .error(function(error) {
            //                     //console.log(error);
            //                     alert('error while loading school details. Please try again later.');
            //                     $ionicLoading.hide();
            //                 });

            //         } else {
            //             alert('error while no response. Please try again later.');
            //             $ionicLoading.hide();
            //             if (callback) {
            //                 callback(false);
            //             }
            //         }
            //     })
            //     .error(function(error) {
            //         //console.log(error);
            //         alert('error while parentExists. Please try again later.');
            //         $ionicLoading.hide();
            //     });
        }
    }
]);