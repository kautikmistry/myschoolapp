angular.module('schoolApp.services', ['schoolApp.config'])

.factory('dataFactory', function($http, $rootScope, Config, $q){
	return {
		isParentExists :function(contactNo){
			return $http.get(Config.BASE_URL +'/api/REST/isParentExists?contactno='+ contactNo);
		},
		verifyParent: function(acesskey, verification_code){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL + '/api/REST/verifyParent?contactno='+ acesskey + '&vericode=' + verification_code);
		},
		signUpParent :function(acesskey, email, password){
			return $http.get(Config.BASE_URL +'/api/REST/signUpParent?contactno='+ acesskey + '&email='+ email +'&pwd=' + password);
		},
		parentLogin : function(contactNo, password){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/parentLogin?contactno='+ contactNo +'&pwd=' + password);
		},
		getParentSchoolStudentData : function(contactNo){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/getParentSchoolStudentData?contactno='+ contactNo);
		},
		getEvents: function(contactNo, schoolID, student_id){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/getEvents?contactno='+ contactNo +'&school_id=' + schoolID +'&student_id=' + student_id);	
		},
		parentExists : function(parentName, contactNo, deviceID){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/Parentexists?pname='+ parentName + '&contactno='+ contactNo +'&deviceid=' + deviceID);
		},
		resendVeriCode : function(contactNo){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/ResendVeriCode?contactno='+ contactNo);
		},
		verifyMobile: function(contactNo, verificationCode){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL + '/api/REST/VerifyMobile?contactno='+ contactNo + '&vericode=' + verificationCode);
		},
		parentDetails: function(contactNo, schoolID){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/ParentDetails?contactno='+ contactNo +'&school=' + schoolID);	
		},
		schoolDetails: function(contactNo, schoolID){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/SchoolDetails?contactno='+ contactNo +'&school=' + schoolID);	
		},
		schoolEvents: function(contactNo, schoolID, orderBy){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/SchoolEvents?contactno='+ contactNo +'&school=' + schoolID + '&order=' + orderBy + ' DESC');	
		},
		eventDetails: function(contactNo, schoolID, eventID){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/EventDetails?contactno='+ contactNo +'&school=' + schoolID + '&event=' + eventID);	
		},
		regDevice: function(contactNo, deviceID){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/regDevice?contactno='+ contactNo +'&deviceid=' + deviceID);	
		},
		contentLike: function(contactNo, event_content_id, flag){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/Contentlike?contactno='+ contactNo + '&contentid=' + event_content_id + '&like=' + flag);	
		},
		reportAbuse: function(contactNo, schoolID, contentID, remarks){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/reportAbuse?contactno='+ contactNo +'&school=' + schoolID + '&contentid=' + contentID + '&remarks=' + remarks);	
		},
		saveInstallationId: function(contactNo, installationID){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/saveInstallationId?contactno='+ contactNo +'&inid=' + installationID + '&plateform=android');	
		},
		getSchools:function(contactNo){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/GetSchools?contactno='+ contactNo);		
		},
		studentgetById: function(arr, student_id){
			var deferred = $q.defer();
		   	var _getById = function(id) {
			   for (var i = 0; i < arr.length; i++) {
                    if (arr[i].student.student_id == student_id) {
                        return arr[i];
                    }
                }
            }
            if (!arr) {
                    deferred.resolve(_getById(student_id))
                
            } else {
                deferred.resolve(_getById(student_id))
            }
            return deferred.promise;
		        
		},
		getStaticMenuContent:function(school_id, page_id){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/getStaticMenuContent?school_id='+ school_id + '&id=' + page_id);		
		},
		getDocuments:function(student_id){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/getDocuments?student='+ student_id);		
		},
		getStudentResult:function(school_id, gr_number, exam){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/getStudentResult?school_id='+ school_id + '&grno=' + gr_number + '&exam=' + exam);		
		},
		getExamsTakenBySchool: function(school_id) {
			return $http.get(Config.BASE_URL +'/api/REST/getExamsTakenBySchool?school_id='+ school_id);
		},
		getEventContent:function(contactNo, event_id){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/getEventContent?contactno='+ contactNo + '&event_id=' + event_id);		
		},
		forgotPassword:function(contactNo){
			// $http.defaults.headers.common['Authorization'] = 'Basic '+$rootScope.config.authorizationKey;
			return $http.get(Config.BASE_URL +'/api/REST/forgotPassword?contactno='+ contactNo);		
		},
		getStudentsMatrials:function(_class){
			return $http.get(Config.BASE_URL +'/api/REST/getStudentsMatrials?class='+ _class);
		},
		getObjectUrl: function(_key){
			return $http.get(Config.BASE_URL +'/api/REST/getObjectUrl?key='+ _key);
		},
		GetAllFaculty : function() {
			return $http.get(Config.BASE_URL +'/api/REST/getAllFaculty');
		},
		receiveMsg : function(Sdid, Rcid) {
			return $http.get(Config.BASE_URL +'/api/REST/receiveMsg?sender_id=' + Sdid + '&receiver_id=' + Rcid	);
		},
		SendMsg :function(msg, Sdid, Rcid, time){
			return $http.get(Config.BASE_URL +'/api/REST/sendMsg?sender_id=' + Rcid + '&receiver_id=' + Sdid + '&msg=' + msg + '&msgtime=' + time	);
		}
		
	}
});