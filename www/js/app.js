// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('schoolApp', ['ionic',
                             'schoolApp.signupCtrl',
                             'schoolApp.signinCtrl',
                             'schoolApp.pagesCtrl',
                             'schoolApp.eventsCtrl',
                             'schoolApp.documentsCtrl',
                             'schoolApp.resultCtrl',
                             'schoolApp.studentsmatrialsCtrl',
                             'schoolApp.messagesCtrl',
                             'schoolApp.messagesDetailCtrl',
                             'schoolApp.studentsCtrl',
                             'schoolApp.controllers',
                             'schoolApp.services',
                             'schoolApp.config',
                             'ngStorage',
                             'ngCordova',
                             'ngSanitize'])
.config(function($ionicConfigProvider) {
  // remove back button text completely
  $ionicConfigProvider.backButton.previousTitleText(false).text(' ');
})

.run(function($ionicPlatform, $ionicPopup, $state, $localStorage) {

    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            
        }
        if (window.StatusBar) {
            StatusBar.hide();
        }
        ionic.Platform.isFullScreen = true;
    // parsePlugin.initialize('J7YvRl6E4y2tVT8cOvBpMLschTtvx4mLPzdVAjX3', 'bt0nJkIDAPCBf07GwGzgbusGaUGhrNUtYcaqnptc', function() {

    //         parsePlugin.subscribe('broadcast', function() {

    //             parsePlugin.getInstallationId(function(id) {

    //                 /*
    //                  * Now you can construct an object and save it to your own services, or Parse, and correlate users to parse installations
    //                  */
    //                  var install_data = {
    //                     installation_id: id,
    //                     channels: ['SampleChannel']
    //                  }
                     
                     
    //                 //$localStorage.installation_id=id;
    //             }, function(e) {
    //                 alert('error');
    //             });

    //         }, function(e) {
    //             alert('error');
    //         });

    //     }, function(e) {
    //         alert('error');
    //     });

        document.addEventListener('deviceready', function () {
        // Enable to debug issues.
        // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

        var notificationOpenedCallback = function(jsonData) {
        console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
        };

        // window.plugins.OneSignal.init("5cac7da0-f299-4dfa-8665-c12181c7cb3a",
        //                          {googleProjectNumber: "899836726479"},
        //                          notificationOpenedCallback);
         window.plugins.OneSignal
            .startInit("5cac7da0-f299-4dfa-8665-c12181c7cb3a")
            .handleNotificationOpened(notificationOpenedCallback)
            .endInit();
        window.plugins.OneSignal.getIds(function(ids) {
        did = ids.userId;
        console.log(did);
        $localStorage.installation_id = ids.userId;
        window.localStorage.setItem("did",ids.userId);
        });
        // Show an alert box if a notification comes in when the user is in your app.
        //window.plugins.OneSignal.enableInAppAlertNotification(true);
        }, false);

        if(window.Connection) {
                if(navigator.connection.type == Connection.NONE) {
                    $ionicPopup.confirm({
                        title: "Internet Disconnected",
                        content: "The internet is disconnected on your device."
                    })
                    .then(function(result) {
                        if(!result) {
                            ionic.Platform.exitApp();
                        }
                    });
                }
            }
        $ionicPlatform.registerBackButtonAction(function (event) {
            console.log($state.current.name);
                if($state.current.name=="app.events" || $state.current.name=="app.pages" || $state.current.name=="app.results" || $state.current.name=="app.contactus" || $state.current.name=="app.documents" ){
                    var confirmPopup = $ionicPopup.confirm({
                        title: 'Alert',
                        template: 'Are you sure you want to Exit the App ?'
                    });
                    confirmPopup.then(function(res) {
                        if(res) {
                            navigator.app.exitApp();
                        } else {
                        console.log('You are not sure');
                        }
                    });
                } else { 
                    navigator.app.backHistory();
                }
            }, 100);
    });
})

.config(function($stateProvider, $urlRouterProvider) {  
    $stateProvider

    .state('signin', {
        url: '/signin',
        cache: false,
        templateUrl: 'templates/signin.html',
        controller: 'signinCtrl'
    })

    .state('signup', {
        url: '/signup',
        cache: false,
        templateUrl: 'templates/signup.html',
        controller: 'signupCtrl'
    })
    
    .state('students', {
        url: '/students',
        cache: false,
        templateUrl: 'templates/students.html',
        controller: 'studentsCtrl'
    })

    .state('app', {
        url: '/app/:student_id',
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
    })

    .state('app.search', {
        url: '/search',
        views: {
            'menuContent': {
                templateUrl: 'templates/search.html'
            }
        }
    })

    .state('app.browse', {
        url: '/browse',
        views: {
            'menuContent': {
                templateUrl: 'templates/browse.html'
            }
        }
    })

    // For Static pages
    .state('app.pages', {
        url: '/pages/:student_id/:school_id/:id',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/pages.html',
                controller: 'pagesCtrl'
            }
        }
    })

    // .state('app.exams', {
    //     url: '/:school_id',
    //     cache: false,
    //     views: {
    //         'menuContent': {
    //             templateUrl: 'templates/exams.html',
    //             controller: 'examsCtrl'
    //         }
    //     }
    // })

    .state('app.events', {
        url: '/events/:student_id/:school_id',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/events.html',
                controller: 'eventsCtrl'
            }
        }
    })

    .state('app.single', {
        url: '/events/:event_id',
        views: {
            '@': {
                templateUrl: 'templates/event.html',
                controller: 'eventCtrl'
            }
        }
    })

    .state('app.results', {
        url: '/results/:school_id/:gr_number',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/result.html',
                controller: 'resultCtrl'
            }
        }
    })

    .state('app.documents', {
        url: '/documents/:student_id',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/documents.html',
                controller: 'documentsCtrl'
            }
        }
    })

    .state('app.studentsmatrials', {
        url: '/studentsmatrials/:class',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/studentsmatrials.html',
                controller: 'studentsmatrialsCtrl'
            }
        }
    })

    .state('app.messages', {
        url: '/messages/:class/:parentid',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/messages.html',
                controller: 'messagesCtrl'
            }
        }
    })

    .state('app.messagesDetail', {
        url: '/messagesDetail/:senderid/:receiverid',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/messagesDetail.html',
                controller: 'messagesDetailCtrl'
            }
        }
    })

    .state('app.aboutschool', {
        url: '/aboutschool/:schoolId',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/aboutschool.html',
                controller: 'aboutschoolCtrl'
            }
        }
    })

    .state('app.aboutactivity', {
        url: '/aboutactivity/:schoolId',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/aboutactivity.html',
                controller: 'aboutactivityCtrl'
            }
        }
    })

    .state('app.aboutmanagement', {
        url: '/aboutmanagement/:schoolId',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/aboutmanagement.html',
                controller: 'aboutmanagementCtrl'
            }
        }
    })

    .state('forgotPassword', {
        url: '/forgotPassword',
        cache: false,
        templateUrl: 'templates/forgotPassword.html',
        controller: 'forgotPasswordCtrl'

    })

    .state('app.contactus', {
        url: '/contactus',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/contactus.html',
                controller: 'contactusCtrl'
            }
        }
    });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/signin');
})

.config(function($provide) {
    $provide.decorator('$state', function($delegate, $stateParams) {
        $delegate.forceReload = function() {
            return $delegate.go($delegate.$current.name, $stateParams, {
                reload: true,
                inherit: false,
                notify: true
            });
        };
        return $delegate;
    });
})

.directive('a', function () {
  return {
    restrict: 'E',
    link: function (scope, element, attrs) {
      if ( !attrs.href ){
        return;
      }
      //console.log("fired");
      var url = attrs.href;
      if ( url.lastIndexOf('http',0) === 0 ){
        element.on('click',function(e){
          e.preventDefault();
          if(attrs.ngClick){
              scope.$eval(attrs.ngClick);
          }
          window.open(encodeURI(url), '_system');
        });
      };
    }
  };
})


.directive('input', function($timeout) {
  return {
    restrict: 'E',
    scope: {
      'returnClose': '=',
      'onReturn': '&',
      'onFocus': '&',
      'onBlur': '&'
    },
    link: function(scope, element, attr) {
      element.bind('focus', function(e) {
        if (scope.onFocus) {
          $timeout(function() {
            scope.onFocus();
          });
        }
      });
      element.bind('blur', function(e) {
        if (scope.onBlur) {
          $timeout(function() {
            scope.onBlur();
          });
        }
      });
      element.bind('keydown', function(e) {
        if (e.which == 13) {
          if (scope.returnClose) element[0].blur();
          if (scope.onReturn) {
            $timeout(function() {
              scope.onReturn();
            });
          }
        }
      });
    }
  }
})

.filter('hrefToJS', function ($sce, $sanitize) {
    return function (text) {
        var regex = /href="([\S]+)"/g;
        var newString = $sanitize(text).replace(regex, "onClick=\"window.open('$1', '_blank', 'location=yes')\"");
        return $sce.trustAsHtml(newString);
    }
})
.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) && angular.isUndefined(key))
            return 0;        
        var sum = 0;        
        angular.forEach(data,function(value){
            sum = sum + parseInt(value[key]);
        });        
        return sum;
    }
})

.filter('trusted', ['$sce',
    function($sce) {
        return function(url) {
            return $sce.trustAsResourceUrl(url);
        };
    }
])

//Convert Html data to plan text()
// .filter('htmlToPlaintext', function() {
//     return function(text){
//         return text ? String(text).replace(/<[^>]+>/gm, '') : '';
//     };
// });

.directive('compileTemplate', function($compile, $parse){
    return {
        link: function(scope, element, attr){
            var parsed = $parse(attr.ngBindHtml);
            function getStringValue() { return (parsed(scope) || '').toString(); }
            
            //Recompile if the template changes
            scope.$watch(getStringValue, function() {
                $compile(element, null, -9999)(scope);  //The -9999 makes it skip directives so that we do not recompile ourselves
            });
        }         
    }
});
