angular.module('schoolApp.controllers', [])


// .controller('signupCtrl', ['$scope', '$rootScope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicLoading', '$ionicHistory', '$cordovaToast',
//     function($scope, $rootScope, $stateParams, dataFactory, Config, $state, $localStorage, $ionicLoading, $ionicHistory, $cordovaToast) {
//         $scope.closeLogin = function() {
//             $ionicHistory.goBack();
//         };
//         $scope.loginData = {};
//         $scope.doLogin = function(userForm) {
//             if (!userForm.$valid) {
//                 //alert('Please enter the details.');
//                 $cordovaToast
//                 .show('Please enter the details!', 'long', 'bottom')
//                     .then(function(success) {

//                         console.log('Error');
//                     });
//                 return;
//             }
//             $ionicLoading.show({
//                 template: 'Verifying Details...'
//             });

//             dataFactory.isParentExists($scope.loginData.contactno)
//                 .success(function(response) {
//                     $scope.acesskey = response.acesskey;
//                     $scope.verification_code = response.verification_code;
//                     dataFactory.verifyParent($scope.acesskey, $scope.verification_code)
//                         .success(function(response) {
//                             console.log("Parent successfully verified");
//                             dataFactory.signUpParent($scope.acesskey, $scope.loginData.email, $scope.loginData.password)
//                             if (response) {
//                                 console.log("Parent Successfully Registerd....");
//                                 console.log($scope.acesskey);
//                                 console.log($scope.loginData.email);
//                                 console.log($scope.loginData.password);
//                                 alert("Parent Successfully Registerd....");
//                                 $ionicLoading.hide();
//                                 $state.go('signin');
//                             } else {
//                                 console.log("You are not a verified parent...");
//                             }

//                         })
//                         .error(function(response) {
//                             //console.log(error);
//                             alert('Does not match verification code.');
//                             $ionicLoading.hide();
//                         })
//                 })
//                 .error(function(response) {
//                     //console.log(error);
//                     alert('parent Could not exists..');
//                     $ionicLoading.hide();
//                 })

//         }
//     }
// ])


.controller('AppCtrl', ['$scope', '$rootScope', '$location', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicPlatform', '$window',
    function($scope, $rootScope, $location, $stateParams, dataFactory, Config, $state, $localStorage, $ionicPlatform, $window) {

        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:
        //$scope.$on('$ionicView.enter', function(e) {
        //});

        $scope.reloadRoute = function() {
             $window.location.reload();
        }

        $scope.AllData = $localStorage.StudentData;
        $scope.stuid = $stateParams.student_id;
        dataFactory.studentgetById($scope.AllData, $scope.stuid).then(function(data) {
            $scope.menuData = data;
            $scope.ParId = $scope.menuData.parent_id;
            if($state.current.controller ==  "AppCtrl") {
                $state.go('app.events', { 'school_id':data.school.school_id, 'student_id':data.student.student_id});
            }else{ }
        });

        
        //console.log($location);
        $scope.doSomething = function(school_id, student_id) {
            $state.go('app.events', { 'school_id':school_id, 'student_id':student_id});
        }   

        $scope.SwitchFuction = function (id, caseStr, school_id, student_id, gr_number, static_id) {
             switch (caseStr) {
                case 'events':
                    $state.go('app.events', { 'school_id':school_id, 'student_id':student_id});
                    break;
                case 'results':
                    $state.go('app.results', { 'school_id':school_id, 'gr_number':gr_number});
                    break;
                // case 'Documents':
                //     $state.go('app.documents', { 'school_id':school_id});
                //     break;
                case 'About School':
                    $state.go('app.pages', { 'school_id':student_id, 'school_id':school_id, 'id':static_id });
                    break;
                case 'About Management':
                    $state.go('app.pages', { 'school_id':student_id, 'school_id':school_id, 'id':static_id });
                    break;
                case 'Contact Us':
                    $state.go('app.pages', { 'school_id':student_id, 'school_id':school_id, 'id':static_id });
                    break;
                case 'Regular Time Table':
                    $state.go('app.pages', { 'school_id':student_id, 'school_id':school_id, 'id':static_id });
                    break;
                case 'Home Work':
                    $state.go('app.pages', { 'school_id':student_id, 'school_id':school_id, 'id':static_id });
                    break;
                case 'Exam Schedule':
                    $state.go('app.pages', { 'school_id':student_id, 'school_id':school_id, 'id':static_id });
                    break;
                case 'Notice Board':
                    $state.go('app.pages', { 'school_id':student_id, 'school_id':school_id, 'id':static_id });
                    break;
                case 'Calendar':
                    $state.go('app.pages', { 'school_id':student_id, 'school_id':school_id, 'id':static_id });
                    break;
                default:
                     $state.go('app.events', { 'school_id':school_id, 'student_id':student_id});
                     break;
            }
        };

        $scope.GetStudentsMatrials = function(id) {
            $state.go('app.studentsmatrials', {'class': id});
        }

        $scope.GetMessages = function(id) {
            $state.go('app.messages', {'class': id});
        }
        
        $scope.BackToStudent = function() {
            //localStorage.removeItem('ngStorage-studentid');
            $state.go('students', {}, {reload: true});
         };

        $scope.setDefault = function(school) {
            $localStorage.StudentData = school;
        }
        $scope.signOut = function() {
            $localStorage.$reset();
            $state.go("signin");
        }
        $scope.$on('refresh', function(event, data) {
            $localStorage.StudentData = data; // 'Some data'
            $scope.StudentData = data;
        });
    }
])

// .controller('signinCtrl', ['$scope', '$rootScope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicLoading', '$cordovaToast',
//     function($scope, $rootScope, $stateParams, dataFactory, Config, $state, $localStorage, $ionicLoading, $cordovaToast) {
//         $scope.singupData = {};
//         // if ($localStorage.schools) {
//         //     dataFactory.getSchools($localStorage.contactno)
//         //         .success(function(response) {
//         //             $ionicLoading.hide();
//         //             //console.log(response.data);
//         //             if (response) {
//         //                 $localStorage.schools = response.data;
//         //                 $localStorage.defaultSchool = response.data[0];
//         //                 params = {
//         //                     'schoolId': $localStorage.defaultSchool.school_id
//         //                 };
//         //                 $state.go('app.events', params)
//         //             } else {
//         //                 alert('error while loading school details. Please try again later.');
//         //                 if (callback) {
//         //                     callback(false);
//         //                 }
//         //             }
//         //         })
//         //         .error(function(response) {
//         //             //console.log(error);
//         //             alert('error while loading school details. Please try again later.');
//         //             $ionicLoading.hide();
//         //         })
//         //     params = {
//         //         'schoolId': $localStorage.defaultSchool.school_id
//         //     };
//         //     $state.go('app.events', params)
//         // }
//         $scope.doSignUp = function(userForm) {
//             if (!userForm.$valid) {
//                 // alert('Please enter the details.');
//                 $cordovaToast
//                 .show('Please enter the details!', 'long', 'bottom')
//                     .then(function(success) {

//                         console.log('Error');
//                     });
//                 return;
//             }
//             $ionicLoading.show({
//                 template: 'Verifying Details...'
//             });

//             dataFactory.isParentExists($scope.singupData.contactno, $scope.singupData.password)
//                 .success(function(response) {
//                     if (response) {
//                         $localStorage.contactNo = response.acesskey;
//                         dataFactory.parentLogin($localStorage.contactNo, $scope.singupData.password)
//                             .success(function(response) {
//                                 console.log("Parent Successfully Loged in");
//                                 $ionicLoading.hide();
//                                 if (response) {
//                                     dataFactory.getParentSchoolStudentData($localStorage.contactNo)
//                                         .success(function(response) {
//                                             if (response) {
//                                                 console.log("Details are found....");
//                                                 $localStorage.StudentData = response.data;
//                                                 console.log($localStorage.StudentData);
//                                                 $state.go('students');

//                                             } else {
//                                                 alert("Sorry No Details Found Please Contact To Administrator..");
//                                             }
//                                         })
//                                         .error(function(error) {
//                                             alert('error while loading school details. Please try again later.');
//                                             $ionicLoading.hide(); 
//                                         });

//                                     // $localStorage.schools = response.data;
//                                     // $localStorage.defaultSchool = response.data[0];
//                                     // params = { 'schoolId': $localStorage.defaultSchool.school_id };
//                                     // $state.go('app.events', params)
//                                 } else {
//                                     alert('error while loading school details. Please try again later.');
//                                     if (callback) {
//                                         callback(false);
//                                     }
//                                 }
//                             })
//                             .error(function(error) {
//                                 //console.log(error);
//                                 alert('error while loading school details. Please try again later.');
//                                 $ionicLoading.hide();
//                             });
//                     } else {

//                     }
//                 })
//                 .error(function(error) {
//                     //console.log(error);
//                     alert('error while parentExists. Please try again later.');
//                     $ionicLoading.hide();
//                 });
//             //console.log(device.uuid);
//             // dataFactory.parentExists($scope.singupData.contactno, $scope.singupData.name, 'Abcdeg169789842dfgjhkhioognbiynkloiyuhkldsfouoiuosdfhiu')
//             //     .success(function(response) {
//             //         $ionicLoading.hide();
//             //         $ionicLoading.show({
//             //           template: 'Loading...'
//             //         });
//             //         console.log('response.data.acesskey:' + response.acesskey);
//             //         if (response) {
//             //             $localStorage.contactno = response.acesskey;
//             //             dataFactory.verifyMobile($localStorage.contactno, 'uhuneb')
//             //                 .success(function(response) {
//             //                     $ionicLoading.hide();
//             //                     //console.log(response.data);
//             //                     if (response) {
//             //                         $localStorage.schools = response.data;
//             //                         $localStorage.defaultSchool = response.data[0];
//             //                         params = { 'schoolId': $localStorage.defaultSchool.school_id };
//             //                         $state.go('app.events', params)
//             //                     } else {
//             //                         alert('error while loading school details. Please try again later.');
//             //                         if (callback) {
//             //                             callback(false);
//             //                         }
//             //                     }
//             //                 })
//             //                 .error(function(error) {
//             //                     //console.log(error);
//             //                     alert('error while loading school details. Please try again later.');
//             //                     $ionicLoading.hide();
//             //                 });

//             //         } else {
//             //             alert('error while no response. Please try again later.');
//             //             $ionicLoading.hide();
//             //             if (callback) {
//             //                 callback(false);
//             //             }
//             //         }
//             //     })
//             //     .error(function(error) {
//             //         //console.log(error);
//             //         alert('error while parentExists. Please try again later.');
//             //         $ionicLoading.hide();
//             //     });
//         }
//     }
// ])

// .controller('studentsCtrl', ['$scope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicLoading', '$rootScope',
//     function($scope, $stateParams, dataFactory, Config, $state, $localStorage, $ionicLoading, $rootScope) {
//         $scope.data = $localStorage.StudentData;
        
//         dataFactory.saveInstallationId($localStorage.contactNo, $localStorage.installation_id)
//             .success(function(response) {

//             })
//             .error(function(error) {
//                 console.log(error);
//                 alert('error while save Installation Id. Please try again later.');
//                 $ionicLoading.hide();
//             });

//     }
// ])

// .controller('eventsCtrl', ['$scope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicLoading', '$rootScope', '$cordovaToast',
//     function($scope, $stateParams, dataFactory, Config, $state, $localStorage, $ionicLoading, $rootScope, $cordovaToast) {
//         $ionicLoading.show({
//             template: 'Loading Events...'
//         });

//         $scope.arr = $localStorage.StudentData;
//         $scope.school_id = $stateParams.school_id
//         console.log("id root", $scope.school_id);
        
//         dataFactory.getEvents($localStorage.contactNo, $scope.school_id)
//         .success(function(response) {
//             $ionicLoading.hide();
//             $scope.events = response.data;
//         })
//         .error(function(error) {
//              $cordovaToast
//             .show('No events Found!', 'long', 'bottom')
//                 .then(function(success) {

//                     console.log('Error');
//                 });
//             $ionicLoading.hide();
//         });

//          $scope.BackToStudent = function() {
//             localStorage.removeItem('ngStorage-studentid');
//             $state.go('students', {}, {reload: true});
//          };
       

//         // dataFactory.saveInstallationId($localStorage.contactno, $localStorage.installation_id)
//         //     .success(function(response) {

//         //     })
//         //     .error(function(error) {
//         //         console.log(error);
//         //         alert('error while save Installation Id. Please try again later.');
//         //         $ionicLoading.hide();
//         //     });

//         // $scope.moveToEvent = function(event) {
//         //     $localStorage.defaultEvent = event;
//         //     $state.go('/events/:schoolId/:eventId', {
//         //         "schoolId": $scope.defaultSchool.school_id,
//         //         "eventId": event.event_id
//         //     });
//         // }
//     }
// ])

.controller('eventCtrl', ['$scope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicLoading', '$ionicPopup', '$rootScope', '$ionicHistory', '$location',
    function($scope, $stateParams, dataFactory, Config, $state, $localStorage, $ionicLoading, $ionicPopup, $rootScope, $ionicHistory, $location) {
        $ionicLoading.show({
            template: 'Loading Event...'
        });
        $scope.goBackevent = function() {
            var entryViewId = _.find($ionicHistory.viewHistory().views, {stateName : "app.events"});
            if(entryViewId) {    
                $location.path(entryViewId.url);
            }

        }
            
        dataFactory.getEventContent($localStorage.contactNo, $stateParams.event_id)
            .success(function(response) {
                $ionicLoading.hide();
                console.log(response);
                $scope.eventData = response.event;
                $scope.eventDetails = response.data; 
                
            })
            .error(function(error) {
                console.log(error);
                alert('error while loading school event details. Please try again later.');
                $ionicLoading.hide();
            });
        $scope.contentLike = function(event_content_id, flag, current) {
            currentState=current.like_or_dislike;
            flag = (flag == currentState ? -1 : flag);
            current.like_or_dislike=flag;
            dataFactory.contentLike($localStorage.contactNo, event_content_id, flag)
                .success(function(response) {
                if (response) {
                } else {
                    alert('error while content Like. Please try again later.');
                    if (callback) {
                        callback(false);
                    }
                }
            })
                .error(function(error) {
                    console.log(error);
                    alert('error while content Like. Please try again later.');
                });
        };
        $scope.reportAbuse = function(event_content_id) {
            console.log($rootScope.school_id);
            var confirmPopup = $ionicPopup.prompt({
                title: 'Alert',
                template: 'Are youy sure, you want Marked it as Report Abuse? </br>leave your comment below'
            });
            confirmPopup.then(function(res) {
                if (res) {
                    console.log(res);
                    dataFactory.reportAbuse($localStorage.contactNo, $rootScope.school_id, event_content_id, res)
                        .success(function(response) {
                            if (response) {
                                $ionicLoading.show({
                                    template: 'Marked as Report Abuse.',
                                    animation: 'fade-in',
                                    showBackdrop: true,
                                    maxWidth: 200,
                                    duration: 2000
                                });
                            } else {
                                alert('error while report Abuse. Please try again later.');
                            }
                        })
                        .error(function(error) {
                            console.log(error);
                            alert('error while report Abuse. Please try again later.');
                        });
                }
            });
        };

        $scope.shareAnywhere = function(detail) {
            // $cordovaSocialSharing.share(detail.name, 'The subject', detail.content);
            window.plugins.socialsharing.share(detail.name, detail.name, detail.content);
        }

        // $scope.openVideo = function (detail) {
        //     alert('\''+ detail.content + '\'');
        //     // $cordovaSocialSharing.share(detail.name, 'The subject', detail.content);
        //     window.open('\''+ detail.content + '\'', '_system');
        // }

    }
])


.controller('aboutschoolCtrl', ['$scope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicLoading',
    function($scope, $stateParams, dataFactory, Config, $state, $localStorage, $ionicLoading) {
        $ionicLoading.show({
            template: 'Loading...'
        });
        dataFactory.schoolDetails($localStorage.contactno, $stateParams.schoolId)
            .success(function(response) {
                $ionicLoading.hide();
                //console.log(response.data);
                if (response) {
                    $scope.defaultSchool = response.data[0];
                } else {
                    alert('error while loading school details about school. Please try again later.');
                    if (callback) {
                        callback(false);
                    }
                }
            })
            .error(function(response) {
                //console.log(error);
                alert('error while loading school details about school. Please try again later.');
                $ionicLoading.hide();
            })
    }
])

.controller('aboutactivityCtrl', ['$scope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicLoading',
    function($scope, $stateParams, dataFactory, Config, $state, $localStorage, $ionicLoading) {
        $ionicLoading.show({
            template: 'Loading...'
        });
        dataFactory.schoolDetails($localStorage.contactno, $stateParams.schoolId)
            .success(function(response) {
                $ionicLoading.hide();
                //console.log(response.data);
                if (response) {
                    $scope.defaultSchool = response.data[0];
                } else {
                    alert('error while loading school details about activity. Please try again later.');
                    if (callback) {
                        callback(false);
                    }
                }
            })
            .error(function(response) {
                //console.log(error);
                alert('error while loading school details about activity. Please try again later.');
                $ionicLoading.hide();
            })
        // $localStorage.schools.forEach(function(item){
        //     if(item.school_id.toString()==$stateParams.schoolId.toString()){
        //         $scope.defaultSchool=item;
        //     }
        // });
    }
])

.controller('aboutmanagementCtrl', ['$scope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$ionicLoading',
    function($scope, $stateParams, dataFactory, Config, $state, $localStorage, $ionicLoading) {
        $ionicLoading.show({
            template: 'Loading...'
        });
        dataFactory.schoolDetails($localStorage.contactno, $stateParams.schoolId)
            .success(function(response) {
                $ionicLoading.hide();
                //console.log(response.data);
                if (response) {
                    $scope.defaultSchool = response.data[0];
                } else {
                    alert('error while loading school details about management. Please try again later.');
                    if (callback) {
                        callback(false);
                    }
                }
            })
            .error(function(response) {
                //console.log(error);
                alert('error while loading school details about management. Please try again later.');
                $ionicLoading.hide();
            })
        // $localStorage.schools.forEach(function(item){
        //     if(item.school_id.toString()==$stateParams.schoolId.toString()){
        //         $scope.defaultSchool=item;
        //     }
        // });
    }
])

.controller('contactusCtrl', ['$scope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$rootScope',
    function($scope, $stateParams, dataFactory, Config, $state, $localStorage, $rootScope) {
        // $scope.defaultSchool=null;
        // $rootScope.$broadcast('refresh', $scope.defaultSchool);
    }
])

.controller('forgotPasswordCtrl', ['$scope', '$stateParams', 'dataFactory', 'Config', '$state', '$localStorage', '$rootScope', '$cordovaToast',
    function($scope, $stateParams, dataFactory, Config, $state, $localStorage, $rootScope, $cordovaToast) {
       
        $scope.ForgotData = {};
        $scope.doForgot = function(userForm) {
            if (!userForm.$valid) {
                // alert('Please enter the details.');
                $cordovaToast
                .show('Please enter the details!', 'long', 'bottom')
                    .then(function(success) {

                        console.log('Error');
                    });
                return;
            } 
        dataFactory.forgotPassword($scope.ForgotData.contactno)
            .success(function(response){
                console.log(response);
                $state.go('signin');
                $cordovaToast
                .show('Reset password mail successfully send at your registered email address!', 'long', 'bottom')
                    .then(function(success) {

                        console.log('Error');
                    });
                    return;
            })
            .error(function(response){
                alert('error while loading school details about activity. Please try again later.');
            })
        }
    }
]);
